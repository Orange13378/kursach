﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kursach
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string DecodeText;
        string EncodeText;
        string m;
        string k;
        public string path;
        bool check;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Encode(object sender, RoutedEventArgs e)
        {
            if (TextBox_Key.Text == null || TextBox_Key.Text == "")
            {
                MessageBox.Show("Ключ не введен");
            }
            else if (TextBox1.Text == "" || TextBox1.Text == null)
            {
                MessageBox.Show("Не выбран файл или ничего не введено");
            }
            else
            {
                check = true;
                m = TextBox1.Text.ToLower();
                EncodeText = new string(Encode(m.ToCharArray(), k.ToCharArray()));
                TextBox2.Text = EncodeText;
            }
        }

        public char[] Encode(char[] message, char[] key) //Метод шифрования 
        {
            int n;
            int d;
            int j, f;
            int t = 0;

            char[] alf = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };

            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < alf.Length; j++)
                {
                    if (message[i] == alf[j])
                    {
                        break;
                    }
                }

                if (j != 33)
                {
                    n = j;

                    if (t > key.Length - 1)
                    {
                        t = 0;
                    }

                    for (f = 0; f < alf.Length; f++)
                    {
                        if (key[t] == alf[f])
                        {
                            break;
                        }
                    }

                    t++;

                    if (f != 33)
                    {
                        d = n + f;
                    }
                    else
                    {
                        d = n;
                    }

                    if (d > 32)
                    {
                        d -= 33;
                    }
                    message[i] = alf[d];
                }
            }
            return message;
        }

        private void Button_Decode(object sender, RoutedEventArgs e)
        {
            if (TextBox_Key.Text == null || TextBox_Key.Text == "")
            {
                MessageBox.Show("Ключ не введен");
            }
            else if (TextBox1.Text == "" || TextBox1.Text == null)
            {
                MessageBox.Show("Не выбран файл или ничего не введено");
            }
            else
            {
                check = false;
                m = TextBox1.Text.ToLower();
                char[] message = Decode(m.ToCharArray(), k.ToCharArray());
                DecodeText = new string(message);
                TextBox2.Text = DecodeText;
            }
        }

        public char[] Decode(char[] message, char[] key) //Метод дешифрования
        {
            int n;
            int d;
            int j, f;
            int t = 0;

            char[] alf = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };

            for (int i = 0; i < message.Length; i++)
            {
                for (j = 0; j < alf.Length; j++)
                {
                    if (message[i] == alf[j])
                    {
                        break;
                    }
                }

                if (j != 33)
                {
                    n = j;

                    if (t > key.Length - 1)
                    {
                        t = 0;
                    }

                    for (f = 0; f < alf.Length; f++)
                    {
                        if (key[t] == alf[f])
                        {
                            break;
                        }
                    }

                    t++;

                    if (f != 33)
                    {
                        d = n - f + alf.Length;
                    }
                    else
                    {
                        d = n;
                    }

                    if (d > 32)
                    {
                        d -= 33;
                    }

                    message[i] = alf[d];
                }
            }
            return message;
        }

        private void Button_OpenFile(object sender, RoutedEventArgs e)
        {
            path = ReadPath(path);

            if (path != (System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Exeption.txt"))
            {
                TextBox1.Text = File.ReadAllText(path, Encoding.Default);
                m = File.ReadAllText(path, Encoding.Default);
            }
        }

        public string ReadPath(string pathc) //Метод для получения пути
        {
            OpenFileDialog o = new OpenFileDialog
            {
                Filter = "txt files (*.txt)|*.txt"
            };

            if (o.ShowDialog() == true)
            {
                path = o.FileName;
                pathc = path;
            }
            else //Обработка если закрыли диологовое окно
            {
                path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Exeption.txt";

                FileInfo info1 = new FileInfo(path);
                if (info1.Exists == false)
                {
                    File.WriteAllText(path, "", Encoding.Default);
                }

                pathc = path;
                MessageBox.Show("Вы не открыли файл!!");
            }

            FileInfo info = new FileInfo(path);

            if (info.Exists == false)
            {
                throw new Exception("This file can not be read. It not exists.\nPath: " + path);
            }
            else
            {
                try
                {
                    return pathc;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private void Button_SaveEncode(object sender, RoutedEventArgs e)
        {
            if (check == true && (TextBox1.Text != null || TextBox1.Text != ""))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                if (saveFileDialog.ShowDialog() == false)
                    return;

                SaveFile(EncodeText, saveFileDialog.FileName);
            }
            else
            {
                MessageBox.Show("Нечего сохранять!!!", "Пустота");
            }
        }

        private void Button_SaveDecode(object sender, RoutedEventArgs e)
        {
            if (check == false && (m != null))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                if (saveFileDialog.ShowDialog() == false)
                    return;

                SaveFile(DecodeText, saveFileDialog.FileName);
            }
            else
            {
                MessageBox.Show("Нечего сохранять!!!", "Пустота");
            }
        }

        public bool SaveFile(string text, string path) //Метод сохранения
        {
            FileInfo info = new FileInfo(path);

            if (info.Exists == false)
            {
                try
                {
                    File.WriteAllText(path + ".txt", text, Encoding.Default);
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                    File.WriteAllText(path, text, Encoding.Default);
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private void TextBox_TextKey(object sender, TextChangedEventArgs e)
        {
            k = Convert.ToString(TextBox_Key.Text.ToLower());
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            TextBox1.IsReadOnly = false;
        }

        private void CheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            TextBox1.IsReadOnly = true;
        }
    }
}